#include "devices.hpp"
#include "detail/device_util.hpp"
#include "detail/device_events_windows.hpp"
#include <windows.h>

namespace peripheral::detail
{
  template <>
  Mapping<GamepadLayout> getMapping([[maybe_unused]] Identity const & identity)
  {
    return {
      .button = {
        rawSignal(Signal::GamepadA), rawSignal(Signal::GamepadB), rawSignal(Signal::GamepadX), rawSignal(Signal::GamepadY),
        rawSignal(Signal::GamepadStart), rawSignal(Signal::GamepadBack),
        rawSignal(Signal::GamepadLeftShoulder), rawSignal(Signal::GamepadRightShoulder),
        rawSignal(Signal::GamepadMode), rawSignal(Signal::GamepadLeftThumb), rawSignal(Signal::GamepadRightThumb),
      },
      .axis = {
        rawSignal(Signal::GamepadLeftTrigger), rawSignal(Signal::GamepadRightTrigger),
      },
      .axis2d = {
        rawSignal(Signal::GamepadLeftStickX), rawSignal(Signal::GamepadLeftStickY),
        rawSignal(Signal::GamepadRightStickX), rawSignal(Signal::GamepadRightStickY),
        rawSignal(Signal::GamepadDpadX), rawSignal(Signal::GamepadDpadY),
      },
    };
  }

  template <>
  Mapping<MouseLayout> getMapping([[maybe_unused]] Identity const & identity)
  {
    return {
      .button = {
        rawSignal(Signal::MouseLeft), rawSignal(Signal::MouseRight), rawSignal(Signal::MouseMiddle), rawSignal(Signal::MouseFwd), rawSignal(Signal::MouseBack),
      },
      .pulse = {
        rawSignal(Signal::MouseWheel), rawSignal(Signal::MouseHWheel),
      },
      .pulse2d = {
        rawSignal(Signal::MouseX), rawSignal(Signal::MouseY),
      },
    };
  }

  template <>
  Mapping<TouchpadLayout> getMapping([[maybe_unused]] Identity const & identity)
  {
    return {
    };
  }

  template <>
  Mapping<KeyboardLayout> getMapping([[maybe_unused]] Identity const & identity)
  {
    return {
      .button = {
        VK_ESCAPE, VK_F1, VK_F2, VK_F3, VK_F4, VK_F5, VK_F6, VK_F7, VK_F8, VK_F9, VK_F10, VK_F11, VK_F12,
        VK_OEM_5, '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', VK_OEM_PLUS, VK_OEM_4, VK_BACK,
        VK_TAB, 'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', VK_OEM_6, VK_OEM_1, VK_RETURN,
        VK_CAPITAL, 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', VK_OEM_3, VK_OEM_7, VK_OEM_2,
        VK_LSHIFT, VK_OEM_102, 'Z', 'X', 'C', 'V', 'B', 'N', 'M', VK_OEM_COMMA, VK_OEM_PERIOD, VK_OEM_MINUS, VK_RSHIFT,
        VK_LCONTROL, VK_LWIN, VK_LMENU, VK_SPACE, VK_RMENU, VK_RWIN, VK_APPS, VK_RCONTROL,
        VK_SNAPSHOT, VK_SCROLL, VK_PAUSE, VK_INSERT, VK_HOME, VK_PRIOR, VK_DELETE, VK_END, VK_NEXT,
        VK_UP, VK_LEFT, VK_DOWN, VK_RIGHT,
        VK_NUMLOCK, VK_DIVIDE, VK_MULTIPLY, VK_SUBTRACT,
        VK_NUMPAD7, VK_NUMPAD8, VK_NUMPAD9, VK_ADD, VK_NUMPAD4, VK_NUMPAD5, VK_NUMPAD6,
        VK_NUMPAD1, VK_NUMPAD2, VK_NUMPAD3, VK_RETURN /* numpad enter */, VK_NUMPAD0, VK_DECIMAL,
      },
    };
  }
}
