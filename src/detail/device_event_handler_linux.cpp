#include "device_event_handler.hpp"

#include "device_data.hpp"

#include <iostream>
#include <libudev.h>
#include <string>

namespace peripheral::detail
{
  class DeviceEventHandlerLinux : public DeviceEventHandler
  {
    public:
      DeviceEventHandlerLinux();
      ~DeviceEventHandlerLinux();
      std::vector<Event> generateInitialEventsForDevice(Identity::DeviceId deviceId) const override;
      bool hasDeviceListChanged() override;
      std::vector<DeviceEvent> getNewEvents() override;
      std::vector<Identity> enumerateDevices() const override;

    private:
      using udev_ptr = udev_ptr_t<udev>;
      using udev_monitor_ptr = udev_ptr_t<udev_monitor>;

      udev_ptr m_udev;
      udev_monitor_ptr m_udev_monitor;
      std::vector<DeviceData> m_devices;
  };
  namespace
  {
    void generateEvents(std::vector<Event> & events, auto const & values, Event::Type type)
    {
      for (Event::SignalType s = 0; s < values.size(); s++)
        events.emplace_back(Event{
            .type = type,
            .signal = s,
            .value = values[s],
          });
    }

    std::vector<DeviceData> getAttachedDevices(udev * udev_)
    {
      std::vector<DeviceData> deviceData;
      udev_enumerate * enumerate = udev_enumerate_new(udev_);
      udev_enumerate_add_match_subsystem(enumerate, "input");
      udev_enumerate_add_match_property(enumerate, "ID_INPUT", "1");
      udev_enumerate_scan_devices(enumerate);
      udev_list_entry * devices = udev_enumerate_get_list_entry(enumerate);

      udev_list_entry * item;
      udev_list_entry_foreach(item, devices)
      {
        char const * name = udev_list_entry_get_name(item);
        if (name)
          deviceData.emplace_back(udev_, name);
      }
      udev_enumerate_unref(enumerate);

      return deviceData;
    }

    bool handleDeviceChanges(udev_monitor * monitor, std::vector<DeviceData> & devices)
    {
      bool changes = false;
      while (udev_device * dev = udev_monitor_receive_device(monitor))
      {
        char const * path = udev_device_get_syspath(dev);
        if (!path)
          continue;

        std::string action = udev_device_get_action(dev);
        if (action == "add")
        {
            devices.emplace_back(udev_monitor_get_udev(monitor), path);
            changes = true;
        }
        else if (action == "remove")
        {
          std::erase_if(devices, [dev](auto const & d){ return d == dev; });
          changes = true;
        }
        std::cerr << action << " " << path << "\n";
      }
      return changes;
    }

    udev_monitor * createUdevMonitor(udev * udev_)
    {
      auto udev_monitor = udev_monitor_new_from_netlink(udev_, "udev");
      udev_monitor_filter_add_match_subsystem_devtype(udev_monitor, "input", nullptr);
      udev_monitor_enable_receiving(udev_monitor);

      return udev_monitor;
    }
  }

  DeviceEventHandlerLinux::DeviceEventHandlerLinux()
    : m_udev(udev_ptr{udev_new(), &udev_unref}),
      m_udev_monitor(udev_monitor_ptr{createUdevMonitor(m_udev.get()), &udev_monitor_unref}),
      m_devices(getAttachedDevices(m_udev.get()))
  {}
  DeviceEventHandlerLinux::~DeviceEventHandlerLinux()
  {}

  std::vector<Event> DeviceEventHandlerLinux::generateInitialEventsForDevice(Identity::DeviceId deviceId) const
  {
    std::vector<Event> events;
    for (auto const & device : m_devices)
    {
      if (device.identity().id == deviceId)
      {
        events.reserve(events.size() + device.state().absolute.size() + device.state().relative.size() + device.state().buttons.size());

        std::cerr << "sending deta: " << "\n";
        generateEvents(events, device.state().absolute, Event::Type::Abs);
        generateEvents(events, device.state().relative, Event::Type::Rel);
        generateEvents(events, device.state().buttons, Event::Type::Btn);
      }
    }

    return events;
  }

  bool DeviceEventHandlerLinux::hasDeviceListChanged()
  {
    return handleDeviceChanges(m_udev_monitor.get(), m_devices);
  }

  std::vector<DeviceEvent> DeviceEventHandlerLinux::getNewEvents()
  {
    std::vector<DeviceEvent> events;

    size_t id = 0;
    for (auto & device : m_devices)
    {
      for (auto const & e : device.readEvents())
        events.emplace_back(DeviceEvent{
            .event = e,
            .deviceId = id,
            });
      id++;
    }
    return events;
  }

  std::vector<Identity> DeviceEventHandlerLinux::enumerateDevices() const
  {
    std::vector<Identity> identities;
    identities.reserve(m_devices.size());
    for (auto const & dev : m_devices)
      identities.emplace_back(dev.identity());
    return identities;
  }

  std::unique_ptr<DeviceEventHandler> createDeviceHandler()
  {
    return std::make_unique<DeviceEventHandlerLinux>();
  }
}
