#include "rawinput.hpp"

#include "device_events_windows.hpp"

#include <algorithm>
#include <cstring>
#include <iostream>
#include <iomanip>
#include <limits>
#include "is_xinput.cpp"

namespace peripheral::detail
{
  namespace
  {
    int32_t scaleValueToInt32(int32_t value, int32_t min, int32_t max)
    {
      int64_t const val = std::clamp(value, min, max);

      if (val < 0 && min != 0)
        return static_cast<int32_t>(val * std::numeric_limits<int32_t>::lowest() / min);
      else if (max != 0)
        return static_cast<int32_t>(val * std::numeric_limits<int32_t>::max() / max);
      else
        return static_cast<int32_t>(val);
    }

    RID_DEVICE_INFO getDeviceInfo(HANDLE rawDeviceHandle)
    {
      RID_DEVICE_INFO deviceInfo;
      deviceInfo.cbSize = sizeof(RID_DEVICE_INFO);

      unsigned int size = sizeof(RID_DEVICE_INFO);
      GetRawInputDeviceInfo(rawDeviceHandle, RIDI_DEVICEINFO, &deviceInfo, &size);

      return deviceInfo;
    }

    std::string getDeviceName(HANDLE rawDeviceHandle)
    {
      std::string name(255, '\0');
      unsigned int size = 255;
      GetRawInputDeviceInfo(rawDeviceHandle, RIDI_DEVICENAME, name.data(), &size);
      return name;
    }

    std::vector<std::byte> getPreparsedData(HANDLE rawDeviceHandle)
    {
      std::vector<std::byte> preparsedDataBuffer;
      unsigned int size;
      GetRawInputDeviceInfo(rawDeviceHandle, RIDI_PREPARSEDDATA, nullptr, &size);
      preparsedDataBuffer.resize(size);
      GetRawInputDeviceInfo(rawDeviceHandle, RIDI_PREPARSEDDATA, preparsedDataBuffer.data(), &size);
      return preparsedDataBuffer;
    }

    HIDP_CAPS getHidCapabilities(std::vector<std::byte> & preparsedDataBuffer)
    {
      auto preparsedData = reinterpret_cast<_HIDP_PREPARSED_DATA*>(preparsedDataBuffer.data());
      HIDP_CAPS hidCapabilities;
      HidP_GetCaps(preparsedData, &hidCapabilities);
      return hidCapabilities;
    }

    std::vector<HIDP_BUTTON_CAPS> getHidButtonCapabilities(std::vector<std::byte> & preparsedDataBuffer, HIDP_CAPS hidCapabilities)
    {
      auto preparsedData = reinterpret_cast<_HIDP_PREPARSED_DATA*>(preparsedDataBuffer.data());
      uint16_t size = hidCapabilities.NumberInputButtonCaps;
      std::vector<HIDP_BUTTON_CAPS> caps(size);
      HidP_GetButtonCaps(HidP_Input, caps.data(), &size, preparsedData);
      return caps;
    }

    std::vector<HIDP_VALUE_CAPS> getHidValueCapabilities(std::vector<std::byte> & preparsedDataBuffer, HIDP_CAPS hidCapabilities)
    {
      auto preparsedData = reinterpret_cast<_HIDP_PREPARSED_DATA*>(preparsedDataBuffer.data());
      uint16_t size = hidCapabilities.NumberInputValueCaps;
      std::vector<HIDP_VALUE_CAPS> caps(size);
      HidP_GetValueCaps(HidP_Input, caps.data(), &size, preparsedData);
      return caps;
    }

    std::optional<Event> updateState(auto & state, Event::SignalType signal, Event::ValueType value, Event::Type type)
    {
      Event::ValueType cur = state[signal];
      if (cur != value)
      {
        state[signal] = value;
        return Event{
          .type = type,
          .signal = signal,
          .value = value,
        };
      }
      return {};
    }

    Identity::DeviceId getId()
    {
      static Identity::DeviceId id;
      return id++;
    }
  }

  DeviceData::DeviceData(HANDLE rawDeviceHandle)
    : m_deviceInfo(getDeviceInfo(rawDeviceHandle)),
      m_name(getDeviceName(rawDeviceHandle))
  {
    m_identity.id = getId();
    m_identity.isOpen = true;
    m_identity.path = m_name;
    if (m_deviceInfo.dwType == RIM_TYPEHID)
    {
      if (IsXInputDevice(m_deviceInfo.hid.dwProductId, m_deviceInfo.hid.dwVendorId))
        m_identity.isOpen = false;
      else
      {
        m_preparsedDataBuffer = getPreparsedData(rawDeviceHandle);
        m_hidCapabilities = getHidCapabilities(m_preparsedDataBuffer);
        m_buttonCaps = getHidButtonCapabilities(m_preparsedDataBuffer, m_hidCapabilities);
        m_valueCaps = getHidValueCapabilities(m_preparsedDataBuffer, m_hidCapabilities);
        for (size_t i = 0; i < m_valueCaps.size(); i++)
        {
          std::cerr << "HID analog usagepage " << m_valueCaps[i].UsagePage <<  "\n";
          std::cerr << " usage:    " << (int)m_valueCaps[i].NotRange.Usage <<  "\n";
          std::cerr << " lusage:   " << (int)m_valueCaps[i].LinkUsage <<  "\n";
          std::cerr << " reportid: " << (int)m_valueCaps[i].ReportID <<  "\n";
          std::cerr << " isrange:  " << (int)m_valueCaps[i].IsRange <<  "\n";
          std::cerr << " isabs:    " << (int)m_valueCaps[i].IsAbsolute <<  "\n";
          std::cerr << " bitfield: " << (int)m_valueCaps[i].BitField <<  "\n";
          std::cerr << " reportct: " << (int)m_valueCaps[i].ReportCount <<  "\n";
          std::cerr << " logical:  " << m_valueCaps[i].LogicalMin << " " << m_valueCaps[i].LogicalMax <<  "\n";
          std::cerr << " physical: " << m_valueCaps[i].PhysicalMin << " " << m_valueCaps[i].PhysicalMax <<  "\n";
          std::cerr << " units:    " << m_valueCaps[i].Units << " " << m_valueCaps[i].UnitsExp <<  "\n";
        }
      }
    }
    switch (m_deviceInfo.dwType)
    {
      case RIM_TYPEMOUSE:
        m_identity.classes = { Identity::Class::Mouse };
        std::cerr << " Mouse " << m_deviceInfo.mouse.dwNumberOfButtons << " " << m_deviceInfo.mouse.fHasHorizontalWheel << "\n";
        break;
      case RIM_TYPEKEYBOARD:
        m_identity.classes = { Identity::Class::Keyboard };
        m_identity.isOpen = m_deviceInfo.keyboard.dwNumberOfKeysTotal > 200; // HACK
        std::cerr << " Keyboard " << m_deviceInfo.keyboard.dwKeyboardMode << " " << m_deviceInfo.keyboard.dwNumberOfKeysTotal << " " << m_deviceInfo.keyboard.dwNumberOfIndicators << "\n";
        break;
      case RIM_TYPEHID:
        m_identity.classes = { Identity::Class::Gamepad };
        std::cerr << " HID " << std::hex << m_deviceInfo.hid.dwVendorId << " " << std::hex << m_deviceInfo.hid.dwProductId << std::dec << "\n";
//              parseHid(rawDeviceHandle);
        break;
    }

  }

  std::vector<Event> DeviceData::parseHidEvent(RAWHID const & event)
  {
    std::vector<Event> events;

    auto preparsedData = reinterpret_cast<_HIDP_PREPARSED_DATA*>(m_preparsedDataBuffer.data());
    for (auto const & buttonCap : m_buttonCaps)
    {
      long unsigned int num = buttonCap.Range.UsageMax - buttonCap.Range.UsageMin + 1;
      std::vector<uint16_t> usage(num);
      std::vector<char> data(event.dwSizeHid);
      std::memcpy(data.data(), event.bRawData, event.dwSizeHid);
      HidP_GetUsages(HidP_Input, buttonCap.UsagePage, 0, usage.data(), &num, preparsedData, data.data(), event.dwSizeHid);

      std::vector<Event::ValueType> values(buttonCap.Range.UsageMax + 1);

      for (size_t i = 0; i < num; i++)
        values[usage[i]] = 1;

      for (Event::SignalType i = 0; i < values.size(); i++)
        if (auto e = updateState(m_state.buttons, i, values[i], Event::Type::Btn))
          events.push_back(*e);
    }
    for (auto const & valueCap : m_valueCaps)
    {
      long unsigned int value;
      std::vector<char> data(event.dwSizeHid);
      std::memcpy(data.data(), event.bRawData, event.dwSizeHid);
      HidP_GetUsageValue(HidP_Input, valueCap.UsagePage, 0, valueCap.NotRange.Usage, &value, preparsedData, data.data(), event.dwSizeHid);

      Event::ValueType v = value;
      if (valueCap.LogicalMin > valueCap.LogicalMax)
        v = scaleValueToInt32(value - 32767, -32768, 32767);

      if (auto e = updateState(m_state.absolute, valueCap.NotRange.Usage, v, Event::Type::Abs))
        events.push_back(*e);
    }
    return events;
  }

  namespace
  {
//    template <typename Handler>
//    int64_t handleRawWindowEvent(HWND__ * hwnd, uint32_t uMsg, uint64_t wParam, int64_t lParam)
//    {
//      switch (uMsg)
//      {
//        case WM_NCCREATE:
//          {
//            CREATESTRUCT * cs = reinterpret_cast<LPCREATESTRUCT>(lParam);
//            auto eventHandler = reinterpret_cast<Handler *>(cs->lpCreateParams);
//            SetWindowLongPtr(hwnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(eventHandler));
//            return true;
//          }
//        case WM_CREATE:
//          {
//            std::array<RAWINPUTDEVICE, 1> rid = {
//              RAWINPUTDEVICE{
//                .usUsagePage = 1,
//                .usUsage     = 0,
//                .dwFlags     = RIDEV_PAGEONLY | RIDEV_INPUTSINK | RIDEV_DEVNOTIFY,
//                .hwndTarget  = hwnd,
//              },
//            };
//
//
//            if(!RegisterRawInputDevices(rid.data(), rid.size(), sizeof(RAWINPUTDEVICE)))
//            {
//              std::cerr << "Failed to RegisterRawInputDevices: " << GetLastError() << "\n";
//              return -1;
//            }
//            return 0;
//          }
//        default:
//          if (auto eventHandler = reinterpret_cast<Handler *>(GetWindowLongPtr(hwnd, GWLP_USERDATA)))
//            return eventHandler->handleWindowEvent(uMsg, wParam, lParam);
//          else
//            return DefWindowProc(hwnd, uMsg, wParam, lParam);
//      }
//    }

    template <typename F>
    WNDCLASSEX createWindowClass(HINSTANCE instance, F && func)
    {
      static const char* class_name = "DUMMY_CLASS";
      WNDCLASSEX wx = {};
      wx.cbSize = sizeof(WNDCLASSEX);
      wx.lpfnWndProc = func;        // function which will handle messages
      wx.hInstance = instance;
      wx.lpszClassName = class_name;
      RegisterClassEx(&wx);
      return wx;
    }

    template <typename T>
    HWND createWindow(WNDCLASSEX windowClass, T * handler)
    {
      auto hwnd = CreateWindowEx(0, windowClass.lpszClassName,
          windowClass.lpszClassName, 0, CW_USEDEFAULT, CW_USEDEFAULT,
          CW_USEDEFAULT, CW_USEDEFAULT, HWND_MESSAGE, nullptr,
          windowClass.hInstance, handler);
      return hwnd;
    }

    void registerMonitoredDevices(HWND hwnd)
    {
      std::array<RAWINPUTDEVICE, 1> rid = {
        RAWINPUTDEVICE{
          .usUsagePage = 1,
          .usUsage     = 0,
          .dwFlags     = RIDEV_PAGEONLY | RIDEV_INPUTSINK | RIDEV_DEVNOTIFY,
          .hwndTarget  = hwnd,
        },
      };

      if(!RegisterRawInputDevices(rid.data(), static_cast<UINT>(rid.size()), sizeof(RAWINPUTDEVICE)))
        std::cerr << "Failed to RegisterRawInputDevices: " << GetLastError() << "\n";
    }

    std::vector<RAWINPUT const *> getEventList(auto buffer, int count)
    {
      RAWINPUT const * event = reinterpret_cast<RAWINPUT*>(buffer.data());

      std::vector<RAWINPUT const *> rawEvents;
      rawEvents.reserve(count);

      for (int i = 0; i < count; i++)
      {
        rawEvents.push_back(event);
        event = NEXTRAWINPUTBLOCK(event);
      }
      return rawEvents;
    }

    void generateButtonEvents(std::vector<Event> & events, Event::SignalType signal, auto buttons, auto down, auto up)
    {
      if (buttons & down)
        events.emplace_back(Event{
         .type = Event::Type::Btn,
         .signal = signal,
         .value = 1,
        });
      if (buttons & up)
        events.emplace_back(Event{
          .type = Event::Type::Btn,
          .signal = signal,
          .value = 0,
        });

    }

    std::vector<Event> parseMouseEvent(RAWMOUSE const & rawMouse)
    {
      std::vector<Event> events;

      if (rawMouse.usFlags & MOUSE_MOVE_ABSOLUTE)
      {
        events.emplace_back(Event{
          .type = Event::Type::Abs,
          .signal = rawSignal(Signal::MouseX),
          .value = scaleValueToInt32(rawMouse.lLastX, 0, 65535),
        });
        events.emplace_back(Event{
          .type = Event::Type::Abs,
          .signal = rawSignal(Signal::MouseY),
          .value = scaleValueToInt32(rawMouse.lLastY, 0, 65535),
        });
      }
      else
      {
        if (rawMouse.lLastX != 0)
          events.emplace_back(Event{
            .type = Event::Type::Rel,
            .signal = rawSignal(Signal::MouseX),
            .value = rawMouse.lLastX,
          });
        if (rawMouse.lLastY != 0)
          events.emplace_back(Event{
            .type = Event::Type::Rel,
            .signal = rawSignal(Signal::MouseY),
            .value = rawMouse.lLastY,
          });
      }

      auto buttons = rawMouse.usButtonFlags;

      generateButtonEvents(events, rawSignal(Signal::MouseLeft), buttons, RI_MOUSE_LEFT_BUTTON_DOWN, RI_MOUSE_LEFT_BUTTON_UP);
      generateButtonEvents(events, rawSignal(Signal::MouseRight), buttons, RI_MOUSE_RIGHT_BUTTON_DOWN, RI_MOUSE_RIGHT_BUTTON_UP);
      generateButtonEvents(events, rawSignal(Signal::MouseMiddle), buttons, RI_MOUSE_MIDDLE_BUTTON_DOWN, RI_MOUSE_MIDDLE_BUTTON_UP);
      generateButtonEvents(events, rawSignal(Signal::MouseBack), buttons, RI_MOUSE_BUTTON_4_DOWN, RI_MOUSE_BUTTON_4_UP);
      generateButtonEvents(events, rawSignal(Signal::MouseFwd), buttons, RI_MOUSE_BUTTON_5_DOWN, RI_MOUSE_BUTTON_5_UP);

      if (buttons & RI_MOUSE_WHEEL)
        events.emplace_back(Event{
          .type = Event::Type::Rel,
          .signal = rawSignal(Signal::MouseWheel),
          .value = static_cast<short>(rawMouse.usButtonData),
        });
      else if (buttons & RI_MOUSE_HWHEEL)
        events.emplace_back(Event{
          .type = Event::Type::Rel,
          .signal = rawSignal(Signal::MouseHWheel),
          .value = static_cast<short>(rawMouse.usButtonData),
        });

      return events;
    }

    Event parseKeyboardEvent(RAWKEYBOARD const & rawKbd)
    {
      return {
        .type = Event::Type::Btn,
        .signal = rawKbd.VKey,
        .value = !(rawKbd.Flags & RI_KEY_BREAK),
      };
    }

    DeviceData * findDevice(std::vector<DeviceData> & devices, HANDLE handle)
    {
      auto name = getDeviceName(handle);
      for (auto & device : devices)
      {
        if (device.matches(name))
          return &device;
      }
      return nullptr;
    }

    std::vector<DeviceEvent> parseEvents(std::vector<RAWINPUT const *> rawEvents, std::vector<DeviceData> & devices)
    {
      std::vector<DeviceEvent> deviceEvents;

      for (auto const rawEvent : rawEvents)
      {
        auto device = findDevice(devices, rawEvent->header.hDevice);

        if (!device)
          continue;
        if (!device->identity().isOpen)
          continue;

        switch (rawEvent->header.dwType)
        {
          case RIM_TYPEMOUSE:
            {
              auto events = parseMouseEvent(rawEvent->data.mouse);
              for (auto const & e : events)
              {
                deviceEvents.emplace_back(DeviceEvent{
                  .event = e,
                  .deviceId = device->identity().id.value(),
                });
//                std::cerr << " Mouse " << e.signal << " " << e.value << "\n";
              }
              break;
            }
          case RIM_TYPEKEYBOARD:
            {
              Event e = parseKeyboardEvent(rawEvent->data.keyboard);
              deviceEvents.emplace_back(DeviceEvent{
                .event = e,
                .deviceId = device->identity().id.value(),
              });
//              std::cerr << " Keyboard " << e.signal << " " << e.value << "\n";
              break;
            }
          case RIM_TYPEHID:
            {
              auto events = device->parseHidEvent(rawEvent->data.hid);
              for (auto const & e : events)
              {
                deviceEvents.emplace_back(DeviceEvent{
                  .event = e,
                  .deviceId = device->identity().id.value(),
                });
//                std::cerr << " HID " << e.signal << " " << e.value << "\n";
              }
            }
            break;
        }
      }

      return deviceEvents;
    }

    std::vector<DeviceData> getAttachedDevices()
    {
      std::vector<DeviceData> deviceData;

      unsigned int numDevices;
      GetRawInputDeviceList(nullptr, &numDevices, sizeof(RAWINPUTDEVICELIST));

      std::vector<RAWINPUTDEVICELIST> rawDevices(numDevices);
      GetRawInputDeviceList(rawDevices.data(), &numDevices, sizeof(RAWINPUTDEVICELIST));

      std::cerr << "Numdevicers: " << numDevices << "\n";
      for (auto const & rawDevice : rawDevices)
      {
        deviceData.emplace_back(rawDevice.hDevice);
      }
      return deviceData;
    }
  }

  RawInputHandler::RawInputHandler()
    : m_instance(GetModuleHandle(nullptr)),
      m_windowClass(createWindowClass(m_instance, DefWindowProc)),
      m_window(createWindow(m_windowClass, this))
  {
    if (!m_window)
      std::cerr << "Failed to create window: " << GetLastError() << "\n";

    registerMonitoredDevices(m_window);
    m_devices = getAttachedDevices();
  }

  std::vector<DeviceEvent> RawInputHandler::pollEvents()
  {
    MSG msg;
    //Process and remove all messages before WM_INPUT
    while(PeekMessage(&msg, nullptr, 0, WM_INPUT - 1, PM_REMOVE))
    {
        std::cerr << "Removing preevents\n";
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }
    //Process and remove all messages after WM_INPUT
    while(PeekMessage(&msg, nullptr, WM_INPUT + 1, std::numeric_limits<unsigned int>::max(), PM_REMOVE))
    {
        std::cerr << "Removing postevents\n";
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    std::array<std::byte, sizeof(RAWINPUT) * 16> buffer;

    unsigned int size = static_cast<unsigned int>(buffer.size());
    int count;

    std::vector<DeviceEvent> events;

    while ((count = GetRawInputBuffer(reinterpret_cast<RAWINPUT*>(buffer.data()), &size, sizeof(RAWINPUTHEADER))) > 0)
    {
      for (auto const & e : parseEvents(getEventList(buffer, count), m_devices))
        events.push_back(e);
    }
    return events;
  }

//  void RawInputHandler::handleWindowEvent(MSG & msg)
//  {
//    switch (msg.message)
//    {
//      case WM_INPUT:
//        std::cerr << "WM_INPUT1\n";
//        break;
//      default:
//        std::cerr << "Got event: " << msg.message << " " << msg.wParam << " " << msg.lParam << "\n";
//        break;
//    }
//    DispatchMessage(&msg);
//  }
}
