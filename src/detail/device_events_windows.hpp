#pragma once

#include "device_util.hpp"

namespace peripheral::detail
{
  enum class Signal : size_t {
    MouseLeft, MouseRight, MouseMiddle, MouseBack, MouseFwd,
    MouseX, MouseY, MouseWheel, MouseHWheel,
    GamepadDpadX, GamepadDpadY, GamepadLeftStickX, GamepadLeftStickY,
    GamepadRightStickX, GamepadRightStickY, GamepadStart, GamepadBack,
    GamepadLeftThumb, GamepadRightThumb, GamepadLeftShoulder,
    GamepadRightShoulder, GamepadA, GamepadB, GamepadX, GamepadY,
    GamepadLeftTrigger, GamepadRightTrigger, GamepadMode,
  };

  inline constexpr Event::SignalType rawSignal(Signal s)
  {
    return static_cast<Event::SignalType>(s);
  }
}
