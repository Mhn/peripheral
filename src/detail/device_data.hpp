#pragma once

#include "device_util.hpp"

#include <array>
#include <linux/input-event-codes.h>
#include <memory>

struct libevdev;
struct udev_monitor;
struct udev_device;
struct udev;

namespace peripheral::detail
{
  struct RawDeviceState
  {
    std::array<Event::ValueType, ABS_CNT> absolute{};
    std::array<Event::ValueType, REL_CNT> relative{};
    std::array<Event::ValueType, KEY_CNT> buttons{};
  };
  
  template <typename T>
  using udev_ptr_t = std::unique_ptr<T, T*(*)(T*)>;
  template <typename T>
  using deleting_ptr = std::unique_ptr<T, void(*)(T*)>;

  class DeviceData
  {
    public:
      DeviceData(udev * udev, char const * name);
      DeviceData(DeviceData const &) = delete;
      DeviceData(DeviceData &&) = default;
      DeviceData & operator=(DeviceData &&) = default;

      std::vector<Event> readEvents();
      RawDeviceState const & state() const { return m_state; }
      Identity const & identity() const { return m_identity; }
      bool operator==(udev_device * dev) const;
    private:
      using udev_device_ptr = udev_ptr_t<udev_device>;
      using evdev_ptr = deleting_ptr<libevdev>;
      udev_device_ptr m_udev_device;
      evdev_ptr m_evdev_device;
      RawDeviceState m_state{};
      Identity m_identity{};
  };

}
