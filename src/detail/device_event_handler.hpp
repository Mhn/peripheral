#pragma once

#include "device_structs.hpp"
#include <memory>
#include <vector>

namespace peripheral::detail
{
  struct DeviceEvent
  {
    Event event;
    Identity::DeviceId deviceId;
  };

  class DeviceEventHandler
  {
    public:
      virtual ~DeviceEventHandler() {};
      virtual std::vector<Event> generateInitialEventsForDevice(Identity::DeviceId deviceId) const = 0;
      virtual bool hasDeviceListChanged() = 0;
      virtual std::vector<DeviceEvent> getNewEvents() = 0;
      virtual std::vector<Identity> enumerateDevices() const = 0;
  };

  std::unique_ptr<DeviceEventHandler> createDeviceHandler();
}

