#include "xinput.hpp"

#include <limits>
#include <Winerror.h>
#include "device_events_windows.hpp"

namespace peripheral::detail
{

  namespace
  {
    int32_t scaleValueToInt32(int32_t value, int32_t min, int32_t max)
    {
      int64_t const val = std::clamp(value, min, max);

      if (val < 0 && min != 0)
        return static_cast<int32_t>(val * std::numeric_limits<int32_t>::lowest() / min);
      else if (max != 0)
        return static_cast<int32_t>(val * std::numeric_limits<int32_t>::max() / max);
      else
        return static_cast<int32_t>(val);
    }

    template <typename T = int16_t>
    void generateAbsEvent(std::vector<Event> & events, Event::SignalType signal, auto prev, auto cur)
    {
      if (prev != cur)
        events.emplace_back(Event{
         .type = Event::Type::Abs,
         .signal = signal,
         .value = scaleValueToInt32(cur,  std::numeric_limits<T>::lowest(),  std::numeric_limits<T>::max()),
        });
    }

    void generateButtonEvents(std::vector<Event> & events, Event::SignalType signal, auto prev, auto cur, auto mask)
    {
      if ((prev & mask) != (cur & mask))
        events.emplace_back(Event{
         .type = Event::Type::Btn,
         .signal = signal,
         .value = cur & mask,
        });
    }

    std::vector<Event> generateEvents(XINPUT_GAMEPAD const & prevState, XINPUT_GAMEPAD const & state)
    {
      std::vector<Event> events;
      generateAbsEvent(events, rawSignal(Signal::GamepadLeftStickX), prevState.sThumbLX, state.sThumbLX);
      generateAbsEvent(events, rawSignal(Signal::GamepadLeftStickY), prevState.sThumbLY, state.sThumbLY);
      generateAbsEvent(events, rawSignal(Signal::GamepadRightStickX), prevState.sThumbRX, state.sThumbRX);
      generateAbsEvent(events, rawSignal(Signal::GamepadRightStickY), prevState.sThumbRY, state.sThumbRY);
      generateAbsEvent<uint8_t>(events, rawSignal(Signal::GamepadLeftTrigger), prevState.bLeftTrigger, state.bLeftTrigger);
      generateAbsEvent<uint8_t>(events, rawSignal(Signal::GamepadRightTrigger), prevState.bRightTrigger, state.bRightTrigger);
      generateButtonEvents(events, rawSignal(Signal::GamepadStart), prevState.wButtons, state.wButtons, XINPUT_GAMEPAD_START);
      generateButtonEvents(events, rawSignal(Signal::GamepadBack), prevState.wButtons, state.wButtons, XINPUT_GAMEPAD_BACK);
      generateButtonEvents(events, rawSignal(Signal::GamepadLeftThumb), prevState.wButtons, state.wButtons, XINPUT_GAMEPAD_LEFT_THUMB);
      generateButtonEvents(events, rawSignal(Signal::GamepadRightThumb), prevState.wButtons, state.wButtons, XINPUT_GAMEPAD_RIGHT_THUMB);
      generateButtonEvents(events, rawSignal(Signal::GamepadLeftShoulder), prevState.wButtons, state.wButtons, XINPUT_GAMEPAD_LEFT_SHOULDER);
      generateButtonEvents(events, rawSignal(Signal::GamepadRightShoulder), prevState.wButtons, state.wButtons, XINPUT_GAMEPAD_RIGHT_SHOULDER);
      generateButtonEvents(events, rawSignal(Signal::GamepadA), prevState.wButtons, state.wButtons, XINPUT_GAMEPAD_A);
      generateButtonEvents(events, rawSignal(Signal::GamepadB), prevState.wButtons, state.wButtons, XINPUT_GAMEPAD_B);
      generateButtonEvents(events, rawSignal(Signal::GamepadX), prevState.wButtons, state.wButtons, XINPUT_GAMEPAD_X);
      generateButtonEvents(events, rawSignal(Signal::GamepadY), prevState.wButtons, state.wButtons, XINPUT_GAMEPAD_Y);

      int dpadX = (state.wButtons & XINPUT_GAMEPAD_DPAD_LEFT) - (state.wButtons & XINPUT_GAMEPAD_DPAD_RIGHT);
      int prev_dpadX = (prevState.wButtons & XINPUT_GAMEPAD_DPAD_LEFT) - (prevState.wButtons & XINPUT_GAMEPAD_DPAD_RIGHT);
      if (prev_dpadX != dpadX)
        events.emplace_back(Event{
         .type = Event::Type::Abs,
         .signal = rawSignal(Signal::GamepadDpadX),
         .value = scaleValueToInt32(dpadX, -1, 1),
        });

      int dpadY = (state.wButtons & XINPUT_GAMEPAD_DPAD_UP) - (state.wButtons & XINPUT_GAMEPAD_DPAD_DOWN);
      int prev_dpadY = (prevState.wButtons & XINPUT_GAMEPAD_DPAD_UP) - (prevState.wButtons & XINPUT_GAMEPAD_DPAD_DOWN);
      if (prev_dpadY != dpadY)
        events.emplace_back(Event{
         .type = Event::Type::Abs,
         .signal = rawSignal(Signal::GamepadDpadY),
         .value = scaleValueToInt32(dpadY, -1,  1),
        });

      return events;
    }
  }

  std::vector<DeviceEvent> XInputHandler::pollEvents()
  {
    std::vector<DeviceEvent> events;

    DWORD dwResult;
    for (DWORD i = 0; i < XUSER_MAX_COUNT; i++ )
    {
      XINPUT_STATE state;

      // Simply get the state of the controller from XInput.
      dwResult = XInputGetState(i, &state);

      if(dwResult == ERROR_SUCCESS)
      {
        if (m_devices[i].dwPacketNumber == state.dwPacketNumber)
          continue;

        for (auto const & e : generateEvents(m_devices[i].Gamepad, state.Gamepad))
          events.emplace_back(DeviceEvent{
            .event = e,
            .deviceId = 10000 * XUSER_MAX_COUNT + i,
          });
        m_devices[i] = state;
        // Controller is connected
      }
      else
      {
        // Controller is not connected
      }
    }
    return events;
  }

  std::vector<Identity> XInputHandler::devices() const
  {
    std::vector<Identity> devices(XUSER_MAX_COUNT);

    DWORD dwResult;
    for (DWORD i = 0; i < XUSER_MAX_COUNT; i++ )
    {
      XINPUT_STATE state;

      // Simply get the state of the controller from XInput.
      dwResult = XInputGetState(i, &state);
      devices[i].path = "xinput";

      if(dwResult == ERROR_SUCCESS)
      {
        devices[i].id = 10000 * XUSER_MAX_COUNT + i;
        devices[i].isOpen = true;
        devices[i].classes = { Identity::Class::Gamepad, };
        // Controller is connected
      }
      else
      {
        devices[i].isOpen = false;
      }
    }

    return devices;
  }
}
