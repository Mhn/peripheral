#pragma once

#ifndef NOMINMAX
#define NOMINMAX 1
#endif

#include <array>
#include "device_structs.hpp"
#include "device_event_handler.hpp"
#include <string>
#include <mfobjects.h> // QWORD
extern "C"
{
  #include <hidsdi.h> // after mfobjects.h
}
#include <windows.h>

namespace peripheral::detail
{
  struct RawDeviceState
  {
    std::array<Event::ValueType, 0xff> absolute{};
    std::array<Event::ValueType, 0xff> relative{};
    std::array<Event::ValueType, 0xff> buttons{};
  };

  class DeviceData
  {
    public:
      DeviceData(HANDLE rawDeviceHandle);

      bool matches(std::string const & name)
      {
        return name == m_name;
      }

      Identity const & identity() const { return m_identity; }
      std::vector<Event> parseHidEvent(RAWHID const & event);
    private:
      HANDLE m_handle;
      RID_DEVICE_INFO m_deviceInfo;
      std::string m_name;
      std::vector<std::byte> m_preparsedDataBuffer;
      HIDP_CAPS m_hidCapabilities;
      std::vector<HIDP_BUTTON_CAPS> m_buttonCaps;
      std::vector<HIDP_VALUE_CAPS> m_valueCaps;
      RawDeviceState m_state;
      Identity m_identity;
  };

  class RawInputHandler
  {
    public:
      RawInputHandler();
//      void handleWindowEvent(MSG & msg);
      std::vector<DeviceEvent> pollEvents();
      std::vector<DeviceData> const & devices() const { return m_devices; }
    private:
      std::vector<DeviceData> m_devices;
      HINSTANCE m_instance;
      WNDCLASSEX m_windowClass;
      HWND m_window;
  };
}
