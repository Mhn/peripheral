#include "device_impl.hpp"
#include <algorithm>
#include <limits>

namespace peripheral::detail
{
  Event::ValueType floatToInt32(float value)
  {
    // If using float in calculations, the precision is too low
    double v = std::clamp(value, -1.0f, 1.0f);

    if (v < 0)
      return static_cast<Event::ValueType>(-v * std::numeric_limits<Event::ValueType>::lowest());
    else
      return static_cast<Event::ValueType>(v * std::numeric_limits<Event::ValueType>::max());
  }

  float int32ToFloat(Event::ValueType value)
  {
    // Do division using double,then cast to float
    // (might be better than dividing with float...?)
    if (value < 0)
      return static_cast<float>(-static_cast<double>(value) / std::numeric_limits<Event::ValueType>::lowest());
    else
      return static_cast<float>(static_cast<double>(value) / std::numeric_limits<Event::ValueType>::max());
  }
}
