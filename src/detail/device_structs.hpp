#pragma once

#include <cstdint>
#include <optional>
#include <string_view>
#include <vector>

namespace peripheral::detail
{
  struct Event
  {
    using SignalType = uint16_t;
    using ValueType = int32_t;

    enum class Type { Abs, Rel, Btn, Led, Ff, Mt };

    Type type;
    SignalType signal;
    ValueType value;
  };

  struct Identity
  {
    using DeviceId = size_t;
    enum class Class { Any, Keyboard, Key, Mouse, Touchpad, Touchscreen, Tablet, Joystick, Accelerometer, Gamepad, Wheel, FlightStick, DancePad, Guitar, DrumKit };

    std::optional<DeviceId> id{};
    bool isOpen{};
    std::vector<Class> classes{};
    std::vector<std::pair<std::string_view, std::string_view>> properties{};
    std::optional<std::string_view> serial{};
    std::optional<std::string_view> path{};
    std::optional<uint16_t> vendorId{};
    std::optional<uint16_t> productId{};
    size_t preferredDeviceIndex{};
  };
}
