#include "device_event_handler.hpp"

#include "rawinput.hpp"
#include "xinput.hpp"

#include <iostream>

namespace peripheral::detail
{
  class DeviceEventHandlerWindows : public DeviceEventHandler
  {
    public:
      DeviceEventHandlerWindows();
      ~DeviceEventHandlerWindows();
      std::vector<Event> generateInitialEventsForDevice(Identity::DeviceId deviceId) const override;
      bool hasDeviceListChanged() override;
      std::vector<DeviceEvent> getNewEvents() override;
      std::vector<Identity> enumerateDevices() const override;

    private:
      RawInputHandler m_rawinputHandler;
      XInputHandler m_xinputHandler;
  };

  DeviceEventHandlerWindows::DeviceEventHandlerWindows()
  {}
  DeviceEventHandlerWindows::~DeviceEventHandlerWindows()
  {}

  std::vector<Event> DeviceEventHandlerWindows::generateInitialEventsForDevice([[maybe_unused]] Identity::DeviceId deviceId) const
  {
    return {};
  }

  bool DeviceEventHandlerWindows::hasDeviceListChanged()
  {
    return false;
  }

  std::vector<DeviceEvent> DeviceEventHandlerWindows::getNewEvents()
  {
    auto events = m_rawinputHandler.pollEvents();
    auto xevents = m_xinputHandler.pollEvents();
    events.insert(events.end(), xevents.begin(), xevents.end());
    return events;
  }

  std::vector<Identity> DeviceEventHandlerWindows::enumerateDevices() const
  {
    std::vector<Identity> identities = m_xinputHandler.devices();
    auto const & devices = m_rawinputHandler.devices();
    identities.reserve(devices.size());
    for (auto const & dev : devices)
      identities.emplace_back(dev.identity());
    return identities;
  }

  std::unique_ptr<DeviceEventHandler> createDeviceHandler()
  {
    return std::make_unique<DeviceEventHandlerWindows>();
  }
}
