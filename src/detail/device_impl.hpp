#pragma once

#include "device_manager.hpp"
#include "device_util.hpp"
#include <iostream>

namespace peripheral::detail
{
  void handleSignal(auto const & mapping, auto & state, Event const & event)
  {
    for (size_t i = 0; i < mapping.size(); i++)
      if (mapping[i] == event.signal)
        state[i] = event.value;
  }

  void handleSignalPulse(auto const & mapping, auto & state, Event const & event)
  {
    for (size_t i = 0; i < mapping.size(); i++)
      if (mapping[i] == event.signal)
        state[i] += event.value;
  }

  template <typename Layout>
  void handleSignalMultiTouch(auto const & mapping, auto & state, Event const & event)
  {
    if constexpr (HasMultiTouch<Layout>)
    {
      if (event.signal == mapping[static_cast<size_t>(Layout::MultiTouch::Slot)])
        state[0] = event.value;
      else
      {
        auto slot = state[0];
//        std::cerr << "slot " << slot << "\n";
        if (event.signal == mapping[static_cast<size_t>(Layout::MultiTouch::TrackingId)])
          state[1 + slot * 4 + 0] = event.value + 1; // offset by one so we can handle 0 as unset
        else if (event.signal == mapping[static_cast<size_t>(Layout::MultiTouch::X)])
          state[1 + slot * 4 + 1] = event.value;
        else if (event.signal == mapping[static_cast<size_t>(Layout::MultiTouch::Y)])
          state[1 + slot * 4 + 2] = event.value;
        else if (event.signal == mapping[static_cast<size_t>(Layout::MultiTouch::Pressure)])
          state[1 + slot * 4 + 3] = event.value;
      }
    }
  }

  void handleSignalButton(auto const & mapping, auto & state, Event const & event)
  {
    bool v = event.value != 0;
    for (size_t i = 0; i < mapping.size(); i++)
      if (mapping[i] == event.signal)
      {
        state[i].up = state[i].up || v;
        state[i].down = state[i].down || !v;
        state[i].state = v;
      }
  }

  template <typename Layout>
  void handleEvent(Mapping<Layout> const & mapping, State<Layout> & state, Event const & event)
  {
//    std::cerr << "got deta: " << (int)event.signal << " "<< event.value << "\n";
    switch(event.type)
    {
      case Event::Type::Mt:
        handleSignalMultiTouch<Layout>(mapping.multitouch, state.multiTouch, event);
        break;
      case Event::Type::Abs:
        handleSignal(mapping.axis, state.axis, event);
        handleSignal(mapping.axis2d, state.axis2d, event);
        break;
      case Event::Type::Rel:
        handleSignalPulse(mapping.pulse, state.pulse, event);
        handleSignalPulse(mapping.pulse2d, state.pulse2d, event);
        break;
      case Event::Type::Btn:
        handleSignalButton(mapping.button, state.button, event);
        break;
      default:
        break;
    }
  }

  template <typename Layout>
  void resetEphemeralState(State<Layout> & state)
  {
    state.pulse.fill(0);
    state.pulse2d.fill(0);
    for (auto & btn : state.button)
      btn.up = btn.down = false;
  }
//
//  std::vector<size_t> selectMatchingDevices(std::vector<Identity> const & devices, Identity const & filter)
//  {
//    return {};
//  }

  Event::ValueType floatToInt32(float value);
  float int32ToFloat(Event::ValueType value);

  template <typename T>
  struct Signal2D
  {
    T x;
    T y;
  };

  template<typename Layout>
  class Reader
  {
    public:
      Reader(State<Layout> state)
        : m_state(state)
      {}

      bool getButton(ButtonFor<Layout> auto signal) const requires HasButtons<Layout>
      {
        return m_state.button[static_cast<size_t>(signal)].state;
      }

      bool getButtonUp(ButtonFor<Layout> auto signal) const requires HasButtons<Layout>
      {
        return m_state.button[static_cast<size_t>(signal)].up;
      }

      bool getButtonDown(ButtonFor<Layout> auto signal) const requires HasButtons<Layout>
      {
        return m_state.button[static_cast<size_t>(signal)].down;
      }

      Event::ValueType getPulse(PulseFor<Layout> auto signal) const requires HasPulses<Layout>
      {
        return m_state.pulse[static_cast<size_t>(signal)];
      }

      float getAxis(AxisFor<Layout> auto signal) const requires HasAxis<Layout>
      {
        return int32ToFloat(m_state.axis[static_cast<size_t>(signal)]);
      }

      Signal2D<Event::ValueType> getPulse2D(Pulse2DFor<Layout> auto signal) const requires HasPulses2D<Layout>
      {
        size_t index = static_cast<size_t>(signal) * 2;
        return {
          .x = m_state.pulse2d[index],
          .y = m_state.pulse2d[index + 1],
        };
      }

      std::optional<Signal2D<float>> getMultiTouch(size_t index) const requires HasMultiTouch<Layout>
      {
        if (m_state.multiTouch[1 + index * 4 + 0] <= 0)
          return {};
        return Signal2D<float>{
          .x = int32ToFloat(m_state.multiTouch[1 + index * 4 + 1]),
          .y = int32ToFloat(m_state.multiTouch[1 + index * 4 + 2]),
        };
      }

      Signal2D<float> getAxis2D(Axis2DFor<Layout> auto signal) const requires HasAxis2D<Layout>
      {
        size_t index = static_cast<size_t>(signal) * 2;
        return {
          .x = int32ToFloat(m_state.axis2d[index]),
          .y = int32ToFloat(m_state.axis2d[index + 1]),
        };
      }
    private:
      State<Layout> m_state;
  };

  template <typename Layout, Filter F = Filter{}>
  class Device
  {
    public:
      using StateReader = Reader<Layout>;

      Device(size_t index = 0, Manager * manager = getDefaultManager())
        : Device(createIdentity<F>(index), manager)
      {}
      Device(Identity identity, Manager * manager = getDefaultManager())
        : m_identity(identity),
          m_manager(manager),
          m_deviceIdentity(manager->getDevice(m_identity)),
          m_mapping(getMapping<Layout>(m_deviceIdentity)),
          m_handle(manager->registerDevice(constructClientData()))
      {}

      Device(Device const &) = delete;
      Device(Device &&) = delete;

      ~Device()
      {
        m_manager->unregisterDevice(m_handle);
      }

      [[nodiscard]] StateReader getUpdatedState()
      {
        m_manager->pollNewEvents();
        auto r = StateReader(m_state);
        resetEphemeralState(m_state);
        return r;
      }

    private:
      Manager::ClientData constructClientData()
      {
        return Manager::ClientData{
          .eventCallback = [this](Event const & e){ handleEventData(e); },
          .deviceCallback = [this]{ reassociateDevice(); },
          .deviceIdentity = m_deviceIdentity,
        };
      }

      void handleEventData(Event const & e)
      {
        handleEvent(m_mapping, m_state, e);
      }

      void reassociateDevice()
      {
        auto const deviceIdentity = m_manager->getDevice(m_deviceIdentity);
        std::cerr << "reassociateDevice " << deviceIdentity.id.value_or(9999) << " " << m_deviceIdentity.id.value_or(9999) << "\n";
        if (deviceIdentity.id != m_deviceIdentity.id)
        {
          m_manager->unregisterDevice(m_handle);
          m_deviceIdentity = deviceIdentity;
          m_mapping = getMapping<Layout>(m_deviceIdentity);
          m_handle = m_manager->registerDevice(constructClientData());
        }
      }

      State<Layout> m_state{};
      Identity m_identity;
      Manager * m_manager;
      Identity m_deviceIdentity;
      Mapping<Layout> m_mapping{};
      size_t m_handle;
  };
}
