#pragma once

#include <array>
#include <functional>
#include <memory>
#include "device_event_handler.hpp"
#include "device_structs.hpp"

namespace peripheral::detail
{
  class Manager
  {
    public:
      using DeviceEventCallback = std::function<void(Event const &)>;
      using DeviceAddedCallback = std::function<void()>;

      struct ClientData
      {
        DeviceEventCallback eventCallback;
        DeviceAddedCallback deviceCallback;
        Identity deviceIdentity;

        bool matchesDeviceId(Identity::DeviceId id)
        {
          return deviceIdentity.id && *deviceIdentity.id == id;
        }
      };

      Manager();
      ~Manager();

      [[nodiscard]] size_t registerDevice(ClientData client);

      void unregisterDevice(size_t index)
      {
        m_clients[index] = {};
      }

      void pollNewEvents();

      std::vector<Identity> enumerateDevices();
      Identity getDevice(Identity const & filter);

    private:
      std::vector<std::optional<ClientData>> m_clients;
      std::unique_ptr<DeviceEventHandler> m_deviceEventHandler;
  };

  Manager * getDefaultManager();
}
