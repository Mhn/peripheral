#include "device_data.hpp"

#include <fcntl.h>
#include <iostream>
#include <libevdev/libevdev.h>
#include <libudev.h>
#include <limits>
#include <span>
#include <unistd.h>

namespace peripheral::detail
{
  namespace
  {
    std::optional<std::string_view> const make_string(char const * s)
    {
      if (s)
        return s;
      return {};
    }

    std::optional<std::string_view> getPropertyValue(auto const & properties, std::string_view name)
    {
      for (auto const & [ key, value ] : properties)
        if (key == name)
          return value;
      return {};
    }

    std::vector<Identity::Class> classifyDevice(udev_device * udev_device, libevdev * evdev_device)
    {
      std::vector<Identity::Class> classes;
      std::array<std::string_view, 8> const classProps = {
        "ID_INPUT_KEYBOARD",
        "ID_INPUT_KEY",
        "ID_INPUT_MOUSE",
        "ID_INPUT_TOUCHPAD",
        "ID_INPUT_TOUCHSCREEN",
        "ID_INPUT_TABLET",
        "ID_INPUT_JOYSTICK",
        "ID_INPUT_ACCELEROMETER",
      };

      udev_list_entry * item;
      udev_list_entry_foreach(item, udev_device_get_properties_list_entry(udev_device))
      {
        std::string_view const p = udev_list_entry_get_name(item);

        for (size_t i = 0; i < classProps.size(); i++)
        {
          if (p == classProps[i])
          {
            if (i == 2)
            {
              // A mouse needs REL_X
              if (evdev_device && !libevdev_has_event_code(evdev_device, EV_REL, REL_X))
                continue;
            }
            classes.push_back(Identity::Class(i));
          }
        }
      }

      if (evdev_device && libevdev_has_event_code(evdev_device, EV_KEY, BTN_GAMEPAD))
        classes.push_back(Identity::Class::Gamepad);

      return classes;
    }

    libevdev * openDevice(udev_device * device)
    {
      auto path = make_string(udev_device_get_devnode(device));

      if (!path || path->empty() || !path->starts_with("/dev/input/event"))
        return nullptr;

      auto fd = ::open(path->data(), O_RDONLY | O_NONBLOCK);
      if (fd < 0)
        return nullptr;

      libevdev * d;
      libevdev_new_from_fd(fd, &d);

      return d;
    }

    RawDeviceState getdeviceState(libevdev * device)
    {
      RawDeviceState state;
      if (device)
      {
        for (Event::SignalType s = 0; s < state.absolute.size(); s++)
          libevdev_fetch_event_value(device, EV_ABS, s, &state.absolute[s]);
        for (Event::SignalType s = 0; s < state.relative.size(); s++)
          libevdev_fetch_event_value(device, EV_REL, s, &state.relative[s]);
        for (Event::SignalType s = 0; s < state.buttons.size(); s++)
          libevdev_fetch_event_value(device, EV_KEY, s, &state.buttons[s]);
        //std::cerr << "fetches la cache " << state.buttons[BTN_SOUTH] << "\n";
      }

      return state;
    }

    void setState(RawDeviceState & state, input_event const & event)
    {
      switch (event.type)
      {
        case EV_ABS:
          state.absolute[event.code] = event.value;
          break;
        case EV_REL:
          state.relative[event.code] = event.value;
          break;
        case EV_KEY:
          state.buttons[event.code] = event.value;
          break;
      }
    }


    Identity getIdentity(udev_device * udev_device, libevdev * evdev_device)
    {
      detail::Identity identity;

      static size_t currentDeviceId = 0;
      identity.id = currentDeviceId++;

//      auto path = make_string(udev_device_get_devnode(device));

//      if (!path || path->empty() || !path->starts_with("/dev/input/event"))
//        return {};

      udev_list_entry * item;
      udev_list_entry_foreach(item, udev_device_get_properties_list_entry(udev_device))
      {
        identity.properties.emplace_back(udev_list_entry_get_name(item), udev_list_entry_get_value(item));
      }
      identity.path = getPropertyValue(identity.properties, "ID_PATH");
      identity.serial = getPropertyValue(identity.properties, "ID_SERIAL");

      identity.classes = classifyDevice(udev_device, evdev_device);
      identity.isOpen = evdev_device != nullptr;
      if (auto v = getPropertyValue(identity.properties, "ID_VENDOR_ID"))
        identity.vendorId = std::strtoul(v->begin(), nullptr, 16);
      if (auto v = getPropertyValue(identity.properties, "ID_MODEL_ID"))
        identity.productId = std::strtoul(v->begin(), nullptr, 16);

      return identity;
    }

    void deleteEvdevDevice(libevdev * dev) noexcept
    {
      if (dev)
      {
        auto fd = libevdev_get_fd(dev);
        libevdev_free(dev);
        ::close(fd);
      }
    }

    std::optional<Event::Type> getType(input_event e)
    {
      switch (e.type)
      {
        case EV_ABS:
          {
            if (e.code > ABS_RESERVED)
              return Event::Type::Mt;
            return Event::Type::Abs;
          }
        case EV_REL:
          return Event::Type::Rel;
        case EV_KEY:
          return Event::Type::Btn;
        default:
          return {};
      }
    }

    int32_t scaleValueToInt32(int32_t value, int32_t min, int32_t max)
    {
      int64_t const val = std::clamp(value, min, max);

      if (val < 0)
        return val * std::numeric_limits<int32_t>::lowest() / min;
      else
        return val * std::numeric_limits<int32_t>::max() / max;
    }

    std::optional<Event> createEvent(libevdev * dev, input_event const & e)
    {
      if (auto type = getType(e))
      {
        input_absinfo const * absinfo = libevdev_get_abs_info(dev, e.code);

        // Don't scale counts
        if (e.code == ABS_MT_SLOT || e.code == ABS_MT_TRACKING_ID)
          absinfo = nullptr;

        return Event{
            .type = *type,
            .signal = e.code,
            .value = absinfo ?
              scaleValueToInt32(e.value, absinfo->minimum, absinfo->maximum) : e.value,
        };
      }
      return {};
    }
  }

  DeviceData::DeviceData(udev * udev, char const * name)
    : m_udev_device(udev_device_ptr{udev_device_new_from_syspath(udev, name), &udev_device_unref}),
      m_evdev_device(evdev_ptr{openDevice(m_udev_device.get()), &deleteEvdevDevice}),
      m_state(getdeviceState(m_evdev_device.get())),
      m_identity(getIdentity(m_udev_device.get(), m_evdev_device.get()))
  {
    if (m_evdev_device)
    {
      std::cerr << "Device created\n";
      for (auto const & [ key, value ] : m_identity.properties)
        std::cerr << " " << key << ": " << value << "\n";
    }
    readEvents();
  }

  std::vector<Event> DeviceData::readEvents()
  {
    if (!m_evdev_device)
      return {};

    std::vector<Event> evts;

    std::array<input_event, 10> eventBuf;
  
    auto fd = libevdev_get_fd(m_evdev_device.get());

    ssize_t bytes{0};
    while ((bytes = ::read(fd, eventBuf.data(), sizeof(eventBuf))) > 0)
    {
      std::span const events = { eventBuf.begin(), std::next(eventBuf.begin(), bytes / sizeof(input_event)) };
      for (auto const & ev : events)
      {
        setState(m_state, ev);

        if (auto e = createEvent(m_evdev_device.get(), ev))
          evts.push_back(*e);
//        if (ev.type == EV_ABS || ev.type == EV_REL || ev.type == EV_KEY)
//          handleDeviceEvent(*it, ev);
      }
    }
//    if (bytes < 0 && errno != EAGAIN && errno != EWOULDBLOCK)
//    {
//      removeFdFromEpoll(m_epollFd, event.data.fd);
//      std::cerr << "error: " << strerror(errno) << std::endl;
//    }
    return evts;
  }

  bool DeviceData::operator==(udev_device * dev) const
  {
    return getIdentity(dev, nullptr).path == m_identity.path;
  }
}
