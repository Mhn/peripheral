#pragma once

#include <array>
#include <concepts>
#include <cstdint>
#include <optional>
#include "device_structs.hpp"

// https://www.kernel.org/doc/Documentation/input/ff.txt

namespace peripheral::detail
{
  template<typename T> concept HasButtons = requires { typename T::Button; };
  template<typename T> concept HasAxis = requires { typename T::Axis; };
  template<typename T> concept HasPulses = requires { typename T::Pulse; };
  template<typename T> concept HasAxis2D = requires { typename T::Axis2D; };
  template<typename T> concept HasPulses2D = requires { typename T::Pulse2D; };
  template<typename T> concept HasLeds = requires { typename T::Led; };
  template<typename T> concept HasForceFeedback = requires { typename T::ForceFeedback; };
  template<typename T> concept HasMultiTouch = requires { typename T::MultiTouch; };
  template<typename U, typename T> concept ButtonFor = std::same_as<typename T::Button, U>;
  template<typename U, typename T> concept AxisFor = std::same_as<typename T::Axis, U>;
  template<typename U, typename T> concept Axis2DFor = std::same_as<typename T::Axis2D, U>;
  template<typename U, typename T> concept PulseFor = std::same_as<typename T::Pulse, U>;
  template<typename U, typename T> concept Pulse2DFor = std::same_as<typename T::Pulse2D, U>;
  template<typename U, typename T> concept ForceFeedbackFor = std::same_as<typename T::ForceFeedback, U>;
  template<typename U, typename T> concept MultiTouchFor = std::same_as<typename T::MultiTouch, U>;

  template <typename T>
  constexpr size_t ButtonCount()
  {
    if constexpr (HasButtons<T>)
      return static_cast<size_t>(T::Button::_Count);
    return 0;
  }

  template <typename T>
  constexpr size_t AxisCount()
  {
    if constexpr (HasAxis<T>)
      return static_cast<size_t>(T::Axis::_Count);
    return 0;
  }

  template <typename T>
  constexpr size_t Axis2DCount()
  {
    if constexpr (HasAxis2D<T>)
      return static_cast<size_t>(T::Axis2D::_Count);
    return 0;
  }

  template <typename T>
  constexpr size_t PulseCount()
  {
    if constexpr (HasPulses<T>)
      return static_cast<size_t>(T::Pulse::_Count);
    return 0;
  }

  template <typename T>
  constexpr size_t Pulse2DCount()
  {
    if constexpr (HasPulses2D<T>)
      return static_cast<size_t>(T::Pulse2D::_Count);
    return 0;
  }

  template <typename T>
  constexpr size_t LedCount()
  {
    if constexpr (HasLeds<T>)
     return static_cast<size_t>(T::Led::_Count);
    return 0;
  }

  template <typename T>
  constexpr size_t ForceFeedbackCount()
  {
    if constexpr (HasForceFeedback<T>)
     return static_cast<size_t>(T::ForceFeedback::_Count);
    return 0;
  }

  template <typename T>
  constexpr size_t MultiTouchCount()
  {
    if constexpr (HasMultiTouch<T>)
     return static_cast<size_t>(T::MultiTouch::_Count);
    return 0;
  }

  template <typename T>
  constexpr size_t MultiTouchFingerCount()
  {
    if constexpr (HasMultiTouch<T>)
     return 5;
    return 0;
  }

  template <typename Layout>
  struct Mapping
  {
    std::array<Event::SignalType, detail::ButtonCount<Layout>()> button{};
    std::array<Event::SignalType, detail::AxisCount<Layout>()> axis{};
    std::array<Event::SignalType, detail::PulseCount<Layout>()> pulse{};
    std::array<Event::SignalType, detail::Axis2DCount<Layout>() * 2> axis2d{};
    std::array<Event::SignalType, detail::Pulse2DCount<Layout>() * 2> pulse2d{};
    std::array<Event::SignalType, detail::LedCount<Layout>()> led{};
    std::array<Event::SignalType, detail::ForceFeedbackCount<Layout>()> forceFeedback{};
    std::array<Event::SignalType, detail::MultiTouchCount<Layout>()> multitouch{};
  };

  template <typename Layout>
  struct State
  {
    struct ButtonValueType { Event::ValueType state : 1, up : 1, down : 1; };
    //struct MultiTouchValueType { Event::ValueType id, x, y, pressure; };

    std::array<ButtonValueType, detail::ButtonCount<Layout>()> button;
    std::array<Event::ValueType, detail::AxisCount<Layout>()> axis;
    std::array<Event::ValueType, detail::PulseCount<Layout>()> pulse;
    std::array<Event::ValueType, detail::Axis2DCount<Layout>() * 2> axis2d;
    std::array<Event::ValueType, detail::Pulse2DCount<Layout>() * 2> pulse2d;
    std::array<Event::ValueType, detail::MultiTouchFingerCount<Layout>() * 4 + 1> multiTouch;
//    std::array<Event::ValueType, detail::LedCount<Layout>()> led;
//    std::array<Event::ValueType, detail::ForceFeedbackCount<Layout>()> forceFeedback;
  };

  template <typename Layout>
  Mapping<Layout> getMapping(Identity const & identity);

  struct Filter
  {
    Identity::Class class_{};
    char const * serial{};
    uint16_t vendorId{};
    uint16_t productId{};
  };

  template <typename T>
  T createValue(auto v, auto c)
  {
    if (v == c)
      return {};
    return { v };
  }

  template <Filter F>
  Identity createIdentity(size_t index)
  {
    return {
      .classes = createValue<std::vector<Identity::Class>>(F.class_, Identity::Class::Any),
      .serial = createValue<std::string_view>(F.serial, nullptr),
      .vendorId = createValue<std::optional<uint16_t>>(F.vendorId, 0),
      .productId = createValue<std::optional<uint16_t>>(F.productId, 0),
      .preferredDeviceIndex = index,
    };
  }
}
