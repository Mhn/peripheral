#include "device_manager.hpp"

#include <iostream>
#include <vector>

namespace peripheral::detail
{

  namespace
  {
    size_t addClientToList(std::vector<std::optional<Manager::ClientData>> & clients, Manager::ClientData client)
    {
      for (size_t i = 0; i < clients.size(); i++)
      {
        if (!clients[i])
        {
          clients[i] = client;
          return i;
        }
      }
      clients.push_back(client);
      return clients.size() - 1;
    }

    bool contains(auto const & haystack, auto const & needle)
    {
      return std::find(haystack.begin(), haystack.end(), needle) != haystack.end();
    }

    bool matchIdentity(Identity const & candidate, Identity const & filter)
    {
//      std::cerr << "Looking for match: " << (int)filter.classes[0] << " " << candidate.path.value_or("none") << "\n";
      if (filter.id && filter.id != candidate.id)
        return false;

      if (!candidate.isOpen)
        return false;

      bool classesMatches = filter.classes.empty();
      for (auto c : filter.classes)
        classesMatches = classesMatches || contains(candidate.classes, c);
      if (!classesMatches)
        return false;

      for (auto const & property : filter.properties)
        if (!contains(candidate.properties, property))
          return false;

      if (filter.path && filter.path != candidate.path)
        return false;

      if (filter.vendorId && filter.vendorId != candidate.vendorId)
        return false;

      if (filter.productId && filter.productId != candidate.productId)
        return false;

      std::cerr << "Match: " << *candidate.path << "\n";
      return true;
    }

    std::optional<Identity> filterDevices(std::vector<Identity> const & devices, Identity const & filter)
    {
      size_t count = 0;
      for (auto const & device : devices)
        if (matchIdentity(device, filter) && count++ == filter.preferredDeviceIndex)
          return device;

      return {};
    }
  }

  Manager::Manager()
    : m_deviceEventHandler(createDeviceHandler())
  {}

  Manager::~Manager()
  {}

  size_t Manager::registerDevice(ClientData client)
  {
    auto index = addClientToList(m_clients, client);
    if (!client.deviceIdentity.id)
      client.deviceIdentity = getDevice(client.deviceIdentity);

    if (client.deviceIdentity.id)
    {
      std::cerr << "Found device for client: " << client.deviceIdentity.id.value() << "\n";
      auto const & events = m_deviceEventHandler->generateInitialEventsForDevice(client.deviceIdentity.id.value());
      for (auto const & e : events)
        client.eventCallback(e);
    }
    return index;
  }

  std::vector<Identity> Manager::enumerateDevices()
  {
    return m_deviceEventHandler->enumerateDevices();
  }

  Identity Manager::getDevice(Identity const & filter)
  {
    return filterDevices(m_deviceEventHandler->enumerateDevices(), filter).value_or(filter);
  }

  void Manager::pollNewEvents()
  {
    if (m_deviceEventHandler->hasDeviceListChanged())
      for (auto & client : m_clients)
        if (client)
          client->deviceCallback();

    auto const & events = m_deviceEventHandler->getNewEvents();
    for (auto const & e : events)
    {
      for (auto & client : m_clients)
        if (client && client->matchesDeviceId(e.deviceId))
          client->eventCallback(e.event);
    }
  }

  Manager * getDefaultManager()
  {
    static Manager instance;
    return &instance;
  }
}
