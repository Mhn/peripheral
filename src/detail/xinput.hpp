#pragma once

#include "device_event_handler.hpp"
#include "device_structs.hpp"
#include <array>
#include <xinput.h>

namespace peripheral::detail
{
  class XInputHandler
  {
    public:
      std::vector<DeviceEvent> pollEvents();
      std::vector<Identity> devices() const;
    private:
      std::array<XINPUT_STATE, XUSER_MAX_COUNT> m_devices;
  };
}
