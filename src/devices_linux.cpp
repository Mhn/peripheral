#include "devices.hpp"
#include "detail/device_util.hpp"
#include <linux/input-event-codes.h>
#include <linux/input.h>

namespace peripheral::detail
{
  template <>
  Mapping<GamepadLayout> getMapping([[maybe_unused]] Identity const & identity)
  {
    return {
      .button = {
        BTN_SOUTH, BTN_EAST, BTN_NORTH, BTN_WEST, BTN_START, BTN_SELECT, BTN_TL, BTN_TR, BTN_MODE, BTN_THUMBL, BTN_THUMBR,
      },
      .axis = {
        ABS_Z, ABS_RZ,
      },
      .axis2d = {
        ABS_X, ABS_Y,
        ABS_RX, ABS_RY,
        ABS_HAT0X, ABS_HAT0Y,
      },
      .forceFeedback = {
        FF_RUMBLE,
        FF_PERIODIC,
        FF_SQUARE,
        FF_TRIANGLE,
        FF_SINE,
        FF_GAIN,
      },
    };
  }

  template <>
  Mapping<MouseLayout> getMapping([[maybe_unused]] Identity const & identity)
  {
    return {
      .button = {
        BTN_LEFT, BTN_RIGHT, BTN_MIDDLE, BTN_FORWARD, BTN_BACK,
      },
      .pulse = {
        REL_WHEEL, REL_WHEEL_HI_RES,
      },
      .pulse2d = {
        REL_X, REL_Y,
      },
    };
  }

  template <>
  Mapping<TouchpadLayout> getMapping([[maybe_unused]] Identity const & identity)
  {
    return {
      .button = {
        BTN_LEFT, BTN_TOUCH, BTN_TOOL_FINGER, BTN_TOOL_DOUBLETAP, BTN_TOOL_TRIPLETAP, BTN_TOOL_QUADTAP, BTN_TOOL_QUINTTAP
      },
      .axis2d = {
        ABS_X, ABS_Y,
      },
      .multitouch = {
        ABS_MT_SLOT, ABS_MT_TRACKING_ID, ABS_MT_POSITION_X, ABS_MT_POSITION_Y, ABS_MT_PRESSURE,
      },
    };
  }

  template <>
  Mapping<KeyboardLayout> getMapping([[maybe_unused]] Identity const & identity)
  {
    return {
      .button = {
        KEY_ESC, KEY_F1, KEY_F2, KEY_F3, KEY_F4, KEY_F5, KEY_F6, KEY_F7, KEY_F8, KEY_F9, KEY_F10, KEY_F11, KEY_F12,
        KEY_GRAVE, KEY_1, KEY_2, KEY_3, KEY_4, KEY_5, KEY_6, KEY_7, KEY_8, KEY_9, KEY_0, KEY_MINUS, KEY_EQUAL, KEY_BACKSPACE,
        KEY_TAB, KEY_Q, KEY_W, KEY_E, KEY_R, KEY_T, KEY_Y, KEY_U, KEY_I, KEY_O, KEY_P, KEY_LEFTBRACE, KEY_RIGHTBRACE, KEY_ENTER,
        KEY_CAPSLOCK, KEY_A, KEY_S, KEY_D, KEY_F, KEY_G, KEY_H, KEY_J, KEY_K, KEY_L, KEY_SEMICOLON, KEY_APOSTROPHE, KEY_BACKSLASH,
        KEY_LEFTSHIFT, KEY_102ND, KEY_Z, KEY_X, KEY_C, KEY_V, KEY_B, KEY_N, KEY_M, KEY_COMMA, KEY_DOT, KEY_SLASH, KEY_RIGHTSHIFT,
        KEY_LEFTCTRL, KEY_LEFTMETA, KEY_LEFTALT, KEY_SPACE, KEY_RIGHTALT, KEY_RIGHTMETA, KEY_COMPOSE, KEY_RIGHTCTRL,
        KEY_SYSRQ, KEY_SCROLLLOCK, KEY_PAUSE, KEY_INSERT, KEY_HOME, KEY_PAGEUP, KEY_DELETE, KEY_END, KEY_PAGEDOWN,
        KEY_UP, KEY_LEFT, KEY_DOWN, KEY_RIGHT,
        KEY_NUMLOCK, KEY_KPSLASH, KEY_KPASTERISK, KEY_KPMINUS,
        KEY_KP7, KEY_KP8, KEY_KP9, KEY_KPPLUS, KEY_KP4, KEY_KP5, KEY_KP6,
        KEY_KP1, KEY_KP2, KEY_KP3, KEY_KPENTER, KEY_KP0, KEY_KPDOT,
      },
      .led = {
        LED_NUML, LED_CAPSL, LED_SCROLLL,
      },
    };
  }
}
