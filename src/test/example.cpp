#include <chrono>
#include <iostream>
#include <thread>
#include "devices.hpp"

void pollDevices()
{
  peripheral::Keyboard keyboard;

  while (true)
  {
    using namespace std::chrono_literals;

    std::this_thread::sleep_for(16ms);

    auto state = keyboard.getUpdatedState();
    if (state.getButtonDown(peripheral::KeyboardLayout::Button::Q))
     std::cerr << "Q\n";
  }
}

int main()
{
  std::thread t(&pollDevices);
  t.join();
  return 0;
}
