#include <array>
#include <chrono>
#include <iomanip>
#include <iostream>
#include <thread>
#include <sstream>
#include <string>
#include <string_view>
#include "devices.hpp"

template <typename Layout>
struct Signalnames
{
  std::array<std::string_view, peripheral::detail::ButtonCount<Layout>()> button{};
  std::array<std::string_view, peripheral::detail::AxisCount<Layout>()> axis{};
  std::array<std::string_view, peripheral::detail::PulseCount<Layout>()> pulse{};
  std::array<std::string_view, peripheral::detail::Axis2DCount<Layout>()> axis2d{};
  std::array<std::string_view, peripheral::detail::Pulse2DCount<Layout>()> pulse2d{};
  std::array<std::string_view, peripheral::detail::LedCount<Layout>()> led{};
};

template <typename Layout>
Signalnames<Layout> getSignalNames();

template <>
Signalnames<peripheral::GamepadLayout> getSignalNames()
{
  return {
    .button = {
      "A", "B", "X", "Y", "S", "B", "Lb", "Rb", "M", "Ls", "Rs",
    },
    .axis = {
      "Lt", "Rt",
    },
    .axis2d = {
      "L", "R", "D",
    },
  };
}

template <>
Signalnames<peripheral::MouseLayout> getSignalNames()
{
  return {
    .button = {
      "L", "R", "M", "F", "B",
    },
    .pulse = {
      "W", "Whi",
    },
    .pulse2d = {
      "P"
    },
  };
}

template <>
Signalnames<peripheral::TouchpadLayout> getSignalNames()
{
  return {
    .button = {
      "C", "T", "1x", "2x", "3x", "4x", "5x",
    },
    .axis2d = {
      "Abs",
    },
  };
}

template <>
Signalnames<peripheral::KeyboardLayout> getSignalNames()
{
  return {
    .button = {
        "Esc", "F1", "F2", "F3", "F4", "F5", "F6", "F7", "F8", "F9", "F10", "F11", "F12",
        "§", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "-", "=", "Bs",
        "Tab", "Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P", "{", "}", "Ent",
        "Cl", "A", "S", "D", "F", "G", "H", "J", "K", "L", ";", "'", "\\",
        "Ls", "102nd", "Z", "X", "C", "V", "B", "N", "M", ",", ".", "/", "Rs",
        "Lc", "Lm", "La", "Sp", "Ra", "Rm", "Cmp", "Rc",
        "Sys", "Scl", "Pb", "Ins", "Hme", "Pgup", "Del", "End", "Pgdn",
        "Up", "Lt", "Dn", "Rt",
        "Nl", "K/", "K*", "K-",
        "K7", "K8", "K9", "K+", "K4", "K5", "K6",
        "K1", "K2", "K3", "Ke", "K0", "K.",
    },
    .led = {
        "LED_NUML", "LED_CAPSL", "LED_SCROLLL",
    },
  };
}

using namespace std::literals;
std::string_view const reset = "\x1B[0m";
std::string_view const on = "\x1B[1;37m";
std::string_view const off = "\x1B[1;30m";
std::string_view const clear = "\033[2J\033[1;1H";
std::string_view const delim = " ";

template <typename T>
void getState(std::stringstream &s, std::string_view const & str, T state);

template <>
void getState(std::stringstream & s, std::string_view const & str, bool state)
{
  s << (state ? on : off) << str << reset;
}

template <>
void getState(std::stringstream & s, std::string_view const & str, peripheral::detail::Event::ValueType state)
{
  s << (std::abs(state) > 0.02f ? on : off) << str << std::setw(5) << state << reset;
}

template <>
void getState(std::stringstream & s, std::string_view const & str, float state)
{
  s.setf(std::ios::fixed); s.setf(std::ios::showpoint); s.precision(2);
  s << (std::abs(state) > 0.02f ? on : off) << str << std::setw(5) << state << reset;
}

template <>
void getState(std::stringstream & s, std::string_view const & str, peripheral::detail::Signal2D<peripheral::detail::Event::ValueType> state)
{
  auto string = std::string(str);
  getState(s, string + "x", state.x);
  getState(s, string + "y", state.y);
}

template <>
void getState(std::stringstream & s, std::string_view const & str, peripheral::detail::Signal2D<float> state)
{
  auto string = std::string(str);
  getState(s, string + "x", state.x);
  getState(s, string + "y", state.y);
}

template <>
void getState(std::stringstream & s, std::string_view const & str, std::optional<peripheral::detail::Signal2D<float>> state)
{
  if (state)
    getState(s, str, *state);
  else
    s << off << str << "x ?.??" << str << "y ?.??" << reset;
}

template <typename Layout>
void printDevice(peripheral::detail::Reader<Layout> const device, std::stringstream & s)
{
  auto const & names = getSignalNames<Layout>();
  if constexpr (auto constexpr count = peripheral::detail::ButtonCount<Layout>(); count > 0)
    for (size_t i = 0; i < count; i++)
      getState(s, names.button[i], device.getButton(typename Layout::Button(i)));
  if constexpr (auto constexpr count = peripheral::detail::AxisCount<Layout>(); count > 0)
    for (size_t i = 0; i < count; i++)
      getState(s, names.axis[i], device.getAxis(typename Layout::Axis(i)));
  if constexpr (auto constexpr count = peripheral::detail::Axis2DCount<Layout>(); count > 0)
    for (size_t i = 0; i < count; i++)
      getState(s, names.axis2d[i], device.getAxis2D(typename Layout::Axis2D(i)));
  if constexpr (auto constexpr count = peripheral::detail::PulseCount<Layout>(); count > 0)
    for (size_t i = 0; i < count; i++)
      getState(s, names.pulse[i], device.getPulse(typename Layout::Pulse(i)));
  if constexpr (auto constexpr count = peripheral::detail::Pulse2DCount<Layout>(); count > 0)
    for (size_t i = 0; i < count; i++)
      getState(s, names.pulse2d[i], device.getPulse2D(typename Layout::Pulse2D(i)));
  if constexpr (auto constexpr count = peripheral::detail::MultiTouchFingerCount<Layout>(); count > 0)
    for (size_t i = 0; i < count; i++)
      getState(s, "Mt" + std::to_string(i), device.getMultiTouch(i));
  s << "\n";
}

void pollDevices()
{
  std::tuple<peripheral::Gamepad, peripheral::Mouse, peripheral::Keyboard, peripheral::Touchpad> devices;

  while (true)
  {
    using namespace std::chrono_literals;

    std::this_thread::sleep_for(16ms);

    std::stringstream s;
    std::apply([&s](auto && ... d){ (printDevice(d.getUpdatedState(), s), ...); }, devices);
    std::cout << clear << s.str();
  }
}

int main()
{
  std::thread t(&pollDevices);
  t.join();
  return 0;
}
