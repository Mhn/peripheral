#include <chrono>
#include <iostream>
#include <thread>
#include <sstream>
#include <string>
#include <string_view>
#include "detail/device_manager.hpp"


using namespace std::literals;
std::string_view const reset = "\x1B[0m";
std::string_view const on = "\x1B[1;37m";
std::string_view const off = "\x1B[1;30m";
std::string_view const clear = "\033[2J\033[1;1H";
std::string_view const delim = " ";

class Device
{
  public:
    void activity()
    {
      m_activity = 120;
    }
    bool hadActivity()
    {
      bool r = m_activity > 0;
      if (r)
        m_activity--;
      return r;
    }
    size_t m_handle;
    peripheral::detail::Identity m_identity;
    int m_activity;
};

void pollDevices()
{
  auto manager = peripheral::detail::getDefaultManager();

  std::vector<Device> devices;
  devices.reserve(manager->enumerateDevices().size());

  for (auto const & id : manager->enumerateDevices())
  {
//    if (!id.isOpen)
//      continue;

    devices.push_back({});
    Device & d = devices.back();
    auto handle = manager->registerDevice({
      .eventCallback = [&d](auto const &){ d.activity(); },
      .deviceCallback = []{},
      .deviceIdentity = id,
    });
    d.m_handle = handle;
    d.m_identity = id;
  }

  while (true)
  {
    using namespace std::chrono_literals;

    std::this_thread::sleep_for(16ms);

    manager->pollNewEvents();

    std::stringstream s;
    for (auto & d : devices)
      s << d.m_identity.isOpen << (d.hadActivity() ? on : off) << d.m_identity.path.value_or("<unknown>") << reset << "\n";
    std::cout << clear << s.str() << "\n";
  }

  for (auto & d : devices)
    manager->unregisterDevice(d.m_handle);
}

int main()
{
  std::thread t(&pollDevices);
  t.join();
  return 0;
}
