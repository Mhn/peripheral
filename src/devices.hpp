#include "detail/device_impl.hpp"

namespace peripheral
{
  struct MouseLayout
  {
    enum class Button { Left, Right, Middle, Forward, Back, _Count };
    enum class Pulse2D { Pointer, _Count };
    enum class Pulse { Scroll, ScrollHiRes, _Count };
  };

  struct TouchpadLayout
  {
    enum class Button { Click, Touch, SingleTouch, DoubleTouch, TripleTouch, QuadTouch, QuintupleTouch, _Count };
    enum class Axis2D { Pointer, _Count };
    enum class MultiTouch { Slot, TrackingId, X, Y, Pressure, _Count };
  };

  struct GamepadLayout
  {
    enum class Button { A, B, X, Y, Start, Back, LeftBumper, RightBumper, Mode, LeftStick, RightStick, _Count };
    enum class Axis2D { Left, Right, DPad, _Count };
    enum class Axis { LeftTrigger, RightTrigger, _Count };
    enum class ForceFeedback { Rumble, Periodic, Square, Triangle, Sine, Gain, _Count };
  };

  struct KeyboardLayout
  {
    enum class Button {
      Escape, F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11, F12,
      Grave, Key1, Key2, Key3, Key4, Key5, Key6, Key7, Key8, Key9, Key0, Minus, Equal, Backspace,
      Tab, Q, W, E, R, T, Y, U, I, O, P, LeftBrace, RightBrace, Enter,
      CapsLock, A, S, D, F, G, H, J, K, L, SemiColon, Apostrophe, Backslash,
      LeftShift, HundredSecond, Z, X, C, V, B, N, M, Comma, Dot, Slash, RightShift,
      LeftControl, LeftMeta, LeftAlt, Space, RightAlt, RightMeta, Compose, RightControl,
      SysRq, ScrollLock, Pause, Insert, Home, PageUp, Delete, End, PageDown,
      Up, Left, Down, Right,
      NumLock, KpSlash, KpAsterisk, KpMinus,
      Kp7, Kp8, Kp9, KpPlus, Kp4, Kp5, Kp6,
      Kp1, Kp2, Kp3, KpEnter, Kp0, KpDot,
      _Count };
    enum class Led { NumLock, CapsLock, ScrollLock, _Count };
  };

  struct OttoLayout
  {
    enum class Button { TopLeft, TopRight, Lower, Front, Side, _Count };
    enum class Axis2D { Stick, _Count };
    enum class Axis { Slider, _Count };
  };

  using Gamepad = detail::Device<GamepadLayout, detail::Filter{ .class_ = detail::Identity::Class::Gamepad }>;
  using LogitechF310 = detail::Device<GamepadLayout, detail::Filter{ .class_ = detail::Identity::Class::Gamepad, .vendorId = 0x046d, .productId = 0xc21d }>;
  using Mouse = detail::Device<MouseLayout, detail::Filter{ .class_ = detail::Identity::Class::Mouse }>;
  using Keyboard = detail::Device<KeyboardLayout, detail::Filter{ .class_ = detail::Identity::Class::Keyboard }>;
  using Touchpad = detail::Device<TouchpadLayout, detail::Filter{ .class_ = detail::Identity::Class::Touchpad }>;
  using Otto = detail::Device<OttoLayout, detail::Filter{ .class_ = detail::Identity::Class::Joystick, .vendorId = 0, .productId = 0 }>;
}
